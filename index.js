

// dummy database
let posts = []
let count = 1

// Add post data
document.querySelector('#form-add-post').addEventListener('submit', (e) => {

	e.preventDefault()
	posts.push({
		id: count,
		title: document.querySelector('#txt-title').value,
		body: document.querySelector('#txt-body').value,
	})

	count++

	showPosts(posts)
	alert("Successfuly Added!")
})

// Show Post
const showPosts = (posts) => {
	let postEntries = ''

	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}" style="display: flex; flex-wrap: wrap">
				<h3 id="post-title-${post.id}" style="padding-right: 3rem">${post.title}</h3>
				<p id="post-body-${post.id}" style="padding-right: 3rem">${post.body}</p><br>
				<button onClick="editPost('${post.id}')">Edit</button>
				<button onClick="deletePost('${post.id}')">Delete</button>
			</div>
		`;
	})

	document.querySelector("#div-post-entries").innerHTML = postEntries
}

// Edit Post
const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML
	let body = document.querySelector(`#post-body-${id}`).innerHTML

	document.querySelector('#txt-edit-id').value = id
	document.querySelector('#txt-edit-title').value = title
	document.querySelector('#txt-edit-body').value = body
}


// Delete Post
const deletePost = (id) => {
	posts = posts.filter((post) => {
		if(post.id.toString() !== id) {
			return post
		}
	})

	document.querySelector(`#post-${id}`).remove()
}

// Update Post

document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
	e.preventDefault()

	for(let i = 0; i < posts.length; i++) {
		// the value post[i].id is a Number while document.querySelector('$txt-edit-edit').value is a String
		// therefore, it is necessary to convert the number to string

		if(posts[i].id.toString() === document.querySelector('#txt-edit-id').value){
			posts[i].title = document.querySelector('#txt-edit-title').value
			posts[i].body = document.querySelector('#txt-edit-body').value

			showPosts(posts)
			alert("Successfuly Updated")
			break;
		}
	}
})


